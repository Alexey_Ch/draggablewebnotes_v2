function EditController($scope, notesService, $routeParams) {
    $scope.note = {};
    $scope.note = notesService.getNote($routeParams.id);

    $scope.addNote = function(note) {
        if(note.title != '') {
            notesService.deleteNote(note.id);
            notesService.addNote(note);
        }
    }
}

function ProfileController($scope, profileService) {
    $scope.profile = profileService.getProfile();

    $scope.save = function(profile){
        profileService.setProfile(profile);
    }
}